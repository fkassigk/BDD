# see https://makefiletutorial.com/

SHELL := /bin/bash -eu -o pipefail
PYTHON_3 ?= python3
PYTHON_D ?= /opt/python.d
SOURCE_PATHS := "src/train_bdd"
CMD_SELENIUM := train_bdd.selenium # :: NOT Implemented yet

VENV_PATH_DEV := $(PYTHON_D)/dev/train/bdd
VENV_PATH_PROD := $(PYTHON_D)/prod/train/bdd

setup_dev: $(VENV_PATH_DEV)


$(VENV_PATH_DEV):
	$(PYTHON_3) -m venv $(VENV_PATH_DEV)
	"$(VENV_PATH_DEV)/bin/pip" install -U pip wheel
	"$(VENV_PATH_DEV)/bin/pip" install -e ".[dev]"

setup_prod: $(VENV_PATH_PROD)

$(VENV_PATH_PROD):
	$(PYTHON_3) -m venv $(VENV_PATH_PROD)
	"$(VENV_PATH_PROD)/bin/pip" install -U pip wheel
	"$(VENV_PATH_PROD)/bin/pip" install .

isort:
	"$(VENV_PATH_DEV)/bin/isort" $(SOURCE_PATHS) tests

pylint:
	"$(VENV_PATH_DEV)/bin/pylint" $(SOURCE_PATHS) tests

coverage_run:
	"$(VENV_PATH_DEV)/bin/coverage" run -m pytest -m "not integration"

coverage_report:
	"$(VENV_PATH_DEV)/bin/coverage" report

mypy:
	"$(VENV_PATH_DEV)/bin/mypy" $(SOURCE_PATHS)

code_check: \
	setup_dev \
	isort \
	pylint \
	coverage_run coverage_report \
	mypy

run_selenium: setup_prod
	source "$(VENV_PATH_PROD)/bin/activate" && $(CMD_SELENIUM)

run_bdd_prod: setup_prod
	source "$(VENV_PATH_PROD)/bin/activate" && behave

run_bdd_dev: setup_dev
	source "$(VENV_PATH_DEV)/bin/activate" && behave "features/04.1 Trusted_Content_Resolver.feature" --tags=-WIP

clean_dev:
	rm -rfv  "$(VENV_PATH_DEV)"

clean_prod:
	rm -rfv  "$(VENV_PATH_PROD)"

activate_env_prod:
	@echo "source \"$(VENV_PATH_PROD)/bin/activate\""

activate_env_dev:
	@echo "source \"$(VENV_PATH_DEV)/bin/activate\""

start-trusted-content-resolver-server-in-debug-mode-prepare:
	mvn -f $$TRAIN_TRUST_CONTENT_RESOLVER_REPO/pom.xml clean install

start-trusted-content-resolver-server-in-debug-mode:
	TCR_DNS_HOSTS= \
	TCR_DID_BASE_URI=http://localhost:8080/1.0 \
	SERVER_PORT=13001 \
		mvn -f $$TRAIN_TRUST_CONTENT_RESOLVER_REPO/service/pom.xml spring-boot:run -Dmaven.surefire.debug

pre-setup:
	cd $$TRAIN_TRUST_CONTENT_RESOLVER_REPO/docker && docker compose --env-file unires.env -f uni-resolver-web.yml up -d
	#cd $$TRAIN_TRUST_CONTENT_RESOLVER_REPO/docker && docker compose up -d

ansible:
	cd deployment/bare_metal && ansible-playbook main.ansible.yaml --connection=local -i localhost,
