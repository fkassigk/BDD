from behave import given, when, then
from behave.runner import Context

from train_bdd.models.domain_name_system_zone_manager import DomainNameSystemZoneManager
from train_bdd.models.trust_framework_pointer import TrustFrameworkPointer


class ContextType(Context):
    dns_zone_manager: DomainNameSystemZoneManager


@given("DNS Zone Manager is up")
def step_impl(context):
    """
    :type context: behave.runner.Context
    """
    context.dns_zone_manager = DomainNameSystemZoneManager()
    assert context.dns_zone_manager.is_up(), 'is not up'


@given("`{exists}` pointer `{trust_framework_pointer_label}:{trust_framework_pointer}`")
def step_impl(context: ContextType,
              exists: str,  # exists or not exist
              trust_framework_pointer_label: str,
              trust_framework_pointer: TrustFrameworkPointer) -> None:

    if exists.startswith('not '):
        assert trust_framework_pointer not in context.dns_zone_manager
    else:
        assert trust_framework_pointer in context.dns_zone_manager

    context.dns_zone_manager[trust_framework_pointer_label] = trust_framework_pointer


@when("creating pointer `{trust_framework_pointer_label}`")
def step_impl(context: ContextType, trust_framework_pointer_label: str) -> None:
    context.http_response = context.dns_zone_manager.create(
        context.dns_zone_manager[trust_framework_pointer_label]
    )


@when("updating pointer `{trust_framework_pointer_label}`")
def step_impl(context: ContextType, trust_framework_pointer_label: str) -> None:
    context.http_response = context.dns_zone_manager.update(
        context.dns_zone_manager[trust_framework_pointer_label]
    )


@when("reading pointer `{trust_framework_pointer_label}`")
def step_impl(context: ContextType, trust_framework_pointer_label: str) -> None:
    context.http_response = context.dns_zone_manager.read(
        context.dns_zone_manager[trust_framework_pointer_label]
    )
