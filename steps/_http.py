"""
Generic BDD steps fpr interpreting HTTP return codes like: 201, 404, 500
"""
from behave import then
import requests

from behave.runner import Context


class ContextType(Context):
    requests_response = requests.Response


@then("get http 200:Success code")
def step_impl(context: ContextType):
    assert context.requests_response == 200


@then("get http 409:Conflict code")
def step_impl(context):
    assert context.requests_response == 409
