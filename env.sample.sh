#!/bin/env bash
# duplicate sample file
# $ cp env.sample.sh env.sh
# configure file
# $ vim env.sh
# start your IDE with new environments
# bash ./env.sh

export TRAIN_TRUST_CONTENT_RESOLVER_HOST="http://localhost:8887"
export TRAIN_TRUST_CONTENT_RESOLVER_CLIENT_PY_VENV="/opt/python.d/dev/train/trusted_content_resolver_client"
export TRAIN_TRUST_CONTENT_RESOLVER_CLIENT_JAVA_TARGET="/Users/app/trusted-content-resolver/clients/java/target/"

# start your IDE with new env
# e.g. IntelliJ
# /Applications/IntelliJ\ IDEA.app/Contents/MacOS/idea
