"""
Trusted content resolver python client wrapper
"""
from typing import cast

from pathlib import Path

import bash

from ..._env import CLIENT_PY_VENV
from ..issuer import Issuer
from ..trust_framework_pointer import TrustFrameworkPointer
from ._base import BaseClient, ResolveResultType

GIT_ROOT = Path(__file__).parent.joinpath("../../../../..").resolve()


class Py(BaseClient):
    """
    BDD step implementation for Python Client
    """
    CLIENT_NAME: str = "trusted-content-resolver-client"
    MOCKED: bool = True

    def resolve(self,
                trust_framework_pointers: set[TrustFrameworkPointer],
                issuer: Issuer) -> ResolveResultType:
        """
        Render example, execute them, process output for assert
        """
        if self.MOCKED:
            return []

        response = bash.bash(f"'{CLIENT_PY_VENV}/bin/tcr' '--arguments ...'")
        assert response.code == 0, response.stderr
        return cast(ResolveResultType, response.stdout.decode())

    def is_up(self) -> bool:
        """
        If it's available in PIP then it's installed.
        """
        output = bash.bash(f"'{CLIENT_PY_VENV}/bin/pip' list | grep {self.CLIENT_NAME}")
        if output.code != 0:
            print(f"FAILED {output.stderr}")
            return False

        return True
