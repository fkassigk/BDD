"""
Trusted content resolver java client wrapper
"""
from typing import cast

import json
from pathlib import Path

import bash

from ..._env import CLIENT_JAVA_TARGET
from ..issuer import Issuer
from ..trust_framework_pointer import TrustFrameworkPointer
from ..trusted_content_resolver import TrustedContentResolver
from ._base import BaseClient, ResolveResultType

GIT_ROOT = Path(__file__).parent.joinpath("../../../../..").resolve()


class Java(BaseClient):
    """
    BDD step implementation for Java Client
    """
    CLIENT_NAME: str = "trusted-content-resolver-java-client"
    backend: TrustedContentResolver

    @property
    def jar_path(self) -> Path:
        """
        Return jar_path
        """
        assert CLIENT_JAVA_TARGET, 'CLIENT_JAVA_TARGET is required'
        paths = list(Path(CLIENT_JAVA_TARGET).glob(f"{self.CLIENT_NAME}*-full.jar"))
        if len(paths) > 1:
            raise RuntimeError(f"Conflicting in multiple version of java client jar, {paths=}")

        if not paths:
            raise RuntimeError(f"No java client can be found in {CLIENT_JAVA_TARGET=}")

        return paths[0]

    def resolve(self,
                trust_framework_pointers: set[TrustFrameworkPointer],
                issuer: Issuer) -> ResolveResultType:
        """
        Call the java client resolve method
        """
        data = {
            "issuer": issuer,
            "trustSchemePointers": list(trust_framework_pointers)
        }

        dump = json.dumps(data)

        client_arguments = [
            f"uri='{self.backend.host}'",
            "endpoint='resolve'",
            f"data='{dump}'"
        ]
        command = f"java -jar {self.jar_path} {' '.join(client_arguments)}"

        response = bash.bash(command)

        output = response.stdout.decode()

        assert response.code == 0, response.stderr
        return cast(ResolveResultType, json.loads(output))

    def is_up(self) -> bool:
        """
        If java `jar with client code available then it can be considered installed
        """
        if not CLIENT_JAVA_TARGET:
            print('Environment CLIENT_JAVA_TARGET not set')
            return False

        return bool(self.jar_path)
