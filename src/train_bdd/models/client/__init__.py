"""
Clients BDD wrapper TCR clients (java, js, py, go)
"""
from ._base import ResolveResultType

__all__ = ["ResolveResultType"]
