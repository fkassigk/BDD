"""All the client will have the same checks"""
from typing import Any, TypeAlias

import abc

from .._service import BaseService
from ..issuer import Issuer
from ..trust_framework_pointer import TrustFrameworkPointer

ResolveResultType: TypeAlias = list[dict[str, Any]]


class BaseClient(BaseService, abc.ABC):  # pylint: disable=too-few-public-methods
    """
    Base Class for java, py, go, js, ...
    """

    @abc.abstractmethod
    def is_up(self) -> bool:
        """Check if client is installed"""

    @abc.abstractmethod
    def resolve(self,
                trust_framework_pointers: set[TrustFrameworkPointer],
                issuer: Issuer) -> ResolveResultType:
        """Call resolve client method"""
