"""
Domain Name System Zone Manager Model
"""
from pydantic import BaseModel


class DomainNameSystemZoneManager(BaseModel):
    """
    Known also as Zone Manager Handler.

    MUST be developed by @Fraunhofer

    See `https://gitlab.eclipse.org/eclipse/xfsc/train/dns-zone-manager`_
    """
    def is_up(self) -> bool:
        """
        To be implemented
        """
        return True
