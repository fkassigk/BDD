"""
Issuer Details Model
"""
from typing import Any

import re


class Issuer(str):
    """
    “IssuerDetails'' describes the DID or URI of the issuer credential
    """
    def __new__(cls, value: str, *_: Any, **__: Any) -> "Issuer":
        """
        Extent the string type
        """
        if value.startswith('did:'):
            # is did
            pass
        elif value.startswith('https://'):
            # is URI https
            pass
        else:
            raise ValueError('Nor DID or URI')

        return super().__new__(cls, value)
