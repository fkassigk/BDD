"""
Trust Framework Pointer Model
"""
from typing import Any


class TrustFrameworkPointer(str):
    """
    DNS pointer record (PTR for short)

    It deviates from the normal usage related to IP
     (https://www.cloudflare.com/learning/dns/dns-records/dns-ptr-record/)
    instead it provides the domain name associated with schema address.

    Example::

        An example dig command to query the PTR record:
            ``dig PTR did-web.test.train.trust-scheme.de``

    """

    def __new__(cls, value: str, *_: Any, **__: Any) -> "TrustFrameworkPointer":
        """
        Extent the string type
        """
        return super().__new__(cls, value)

    @classmethod
    def from_text(cls, text: str) -> set["TrustFrameworkPointer"]:
        """
        Parse multiline test to extract Trust Framework Pointers
        """
        return set(cls(host) for host in text.splitlines())
