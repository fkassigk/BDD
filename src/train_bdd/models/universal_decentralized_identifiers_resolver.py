"""
Universal Decentralized Identifiers Resolver Model
"""
import pydantic
import requests

from .._defaults import REQUESTS_TIMEOUT_IN_SECONDS
from .._env import DID_RESOLVER_HOST
from ._spring_boot_actuator import SpringBootActuator


class UniversalDecentralizedIdentifiersResolver(SpringBootActuator):
    """
    Universal Decentralized Identifiers Resolver is a REST api.

    See `https://github.com/decentralized-identity/universal-resolver`_
    """

    host: pydantic.HttpUrl = pydantic.HttpUrl(DID_RESOLVER_HOST or "http://localhost:8080/")

    def is_up(self) -> bool:
        if str(self.host).startswith("https://dev.uniresolver.io"):
            response = requests.get(
                url="https://dev.uniresolver.io/testIdentifiers",
                timeout=REQUESTS_TIMEOUT_IN_SECONDS
            )

            return response.status_code == 200

        return super().is_up()
