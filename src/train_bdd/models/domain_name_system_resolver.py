"""
Domain Name System Resolver Model
"""
from unittest import mock

import dns.resolver
from pydantic import BaseModel

from .trust_framework_pointer import TrustFrameworkPointer


class DomainNameSystemResolver(BaseModel):
    """
    Pre-configurable DNS servers wrapper for:
    - KNOT
    - NSD
    """

    implementation: str
    ip: str
    TEST_QNAME: str = "_scheme._trust.did-web.test.train.trust-scheme.de"
    resolver: dns.resolver.Resolver

    class Config:  # pylint: disable= too-few-public-methods
        """Pydantic workaround for type dns.resolver.Resolver"""
        arbitrary_types_allowed = True

    @classmethod
    def init(cls, implementation: str, ip: str) -> "DomainNameSystemResolver":
        """
        Factory method which properly initiate API for the lib `dns.resolver`

        :param implementation: DNS implementation name
        :param ip: internet protocol e.g. 127.0.0.1
        """
        if " Mocked " in implementation:
            mocked = mock.Mock(implementation=implementation, ip=ip)
            mocked.is_up.return_value = True

            if implementation == "Configured Mocked DNS":
                mocked.configured_resolve_trust_framework_pointer.return_value = True
                return mocked

            if implementation == "Misconfigured Mocked DNS":
                mocked.configured_resolve_trust_framework_pointer.return_value = False
                return mocked

            raise NotImplementedError(implementation)

        resolver = dns.resolver.Resolver()
        resolver.nameservers = [
            ip
        ]
        return cls(
            implementation=implementation,
            ip=implementation,
            resolver=resolver,
        )

    def _resolve(self, host: str, resource_records: str) -> object:
        """
        Query nameservers to find if it can answer to the simplest question.
        """
        data = []

        response = self.resolver.resolve(host, resource_records)

        for rdata in response:
            data.append(rdata)

        return data

    def is_up(self) -> bool:
        """
        dns.resolver.LifetimeTimeout if the DNS server is no accessible
        """
        try:
            self._resolve(
                self.TEST_QNAME,
                "CNAME"
            )
        except dns.resolver.NoNameservers as exc:
            print(exc)
            return False
        except dns.resolver.LifetimeTimeout as exc:
            print(exc)
            return False

        return True

    def configured_resolve_trust_framework_pointer(self, tfp: TrustFrameworkPointer) -> bool:
        """Is Trust Framework Pointer configured"""
        try:
            self._resolve(
                tfp,
                "PTR"
            )
        except dns.resolver.NoNameservers as exc:
            print(exc)
            return False
        except dns.resolver.LifetimeTimeout as exc:
            print(exc)
            return False
        except dns.rdatatype.UnknownRdatatype as exc:
            print(exc)
            return False
        return True

    @property
    def security_extensions(self) -> bool:
        """
        # Once DNS SEC is configured in CI/CD we should resume BDD implementation
        # @Fraunhofer are suggesting to check the flag in the response

        # Passing commands to `dig` tool and see the results
        #
        #  more info how to here
        #  https://www.cyberciti.biz/faq/unix-linux-test-and-validate-dnssec-using-dig-command-line/
        #
        """
        return True
