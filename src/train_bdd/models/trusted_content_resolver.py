"""
Trusted Content Resolver Model
"""
from typing import cast

import pydantic
import requests
import requests.exceptions

from .._defaults import REQUESTS_TIMEOUT_IN_SECONDS
from .._env import HOST
from ._spring_boot_actuator import SpringBootActuator
from .client import ResolveResultType
from .issuer import Issuer
from .trust_framework_pointer import TrustFrameworkPointer


class TrustedContentResolver(SpringBootActuator):
    """
    Trusted Content Resolver / Extended Universal Resolver (TCR for short) + Libraries

    See `https://gitlab.eclipse.org/eclipse/xfsc/train/trusted-content-resolver/-/blob/main/openapi/tcr_openapi.yaml`_
    """

    host: pydantic.HttpUrl = pydantic.HttpUrl(HOST or "http://localhost:8087")

    def resolve(self,
                issuer: Issuer,
                trust_framework_pointers: set[TrustFrameworkPointer]) -> ResolveResultType:
        """
        Invoke `resolve` REST endpoint
        """
        response = requests.post(
            url=f"{self.host}resolve",
            json={
                'issuer': issuer,
                'trustSchemePointer': list(trust_framework_pointers)
            },
            headers={
                'Content-Type': "application/json",
            },
            timeout=REQUESTS_TIMEOUT_IN_SECONDS,
        )
        assert response.status_code == 200
        return cast(ResolveResultType, response.json())
