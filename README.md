# TRAIN Behavior-driven development (BDD) framework  

Based on python's based BDD lib [Behave]

# Description

TODO: provide picture with explanation about the design.

# Getting started

## Requirements

* Dependents clients/servers setup from TRAIN's components e.g.:
    + [dns-zone-manager]
    + [trusted-content-resolver]
    + [tspa]
* **python3.11/behave** - require to run BDD features files. 
* Optional **make**: tool which controls the generation of executables and other non-source files of a 
program from the program's source files.
On *Linux/MacOS* is available by default, and for *Windows* see https://stackoverflow.com/questions/2532234/how-to-run-a-makefile-in-windows

## Native setup

Bare metal setup on MacOS/Linux is scripted with ansible.
Important (!) MacOS ansible script will require homebrew. 

### Provide python3.11

``` bash
$ make ansible
cd deployment/bare_metal && ansible-playbook main.ansible.yaml --connection=local -i localhost,
...
```

### Run behave

``` bash
# if your default python version is not 3.11 you have to tell which version from to create venv
export PYTHON_3=/opt/path/to/python3.11
$ make run_bdd_dev
# create venv
# install all pip dependencies
...
behave
...
```

Windows users are suggested to [setup with docker](##Setup-with-docker)

Use environment helper for your IDE [env.sample.sh](env.sample.sh).

## Setup with docker

### Docker build

``` bash
# TODO make docker-build # will create the image
```

### Docker run

``` bash
# TODO make docker-run # will mount repo and run behave
```

## License

Apache License  Version 2.0 see [LICENSE](LICENSE).

[dns-zone-manager]: https://gitlab.eclipse.org/eclipse/xfsc/train/dns-zone-manager
[trusted-content-resolver]: https://gitlab.eclipse.org/eclipse/xfsc/train/trusted-content-resolver
[tspa]: https://gitlab.eclipse.org/eclipse/xfsc/train/tspa
[Behave]: https://behave.readthedocs.io
